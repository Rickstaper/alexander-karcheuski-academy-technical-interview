using System.IO;


namespace Rover
{
    public class GlobalRover
    {
        static string path = Path.Combine (System.Environment.CurrentDirectory, "path-plan.txt");
        /// <summary>
        /// FirstFuel, SecondFuel - fuel from point [0,0]
        /// </summary>
        static int FirstFuel { get; set; }
        static int DiagonaleMovesInFirstWay { get; set; }
        static int SecondFuel { get; set; }
        static int DiagonaleMovesInSecondWay { get; set; }
        static int ThirdFuel { get; set; }
        static int DiagonaleMovesInThirdWay { get; set; }
        /// <summary>
        /// ThirdFuel, FourthFuel - fuel from point [3,3]
        /// </summary>
        static int FourthFuel { get; set; }
        static int DiagonaleMovesInFourthWay { get; set; }
        static int FifthFuel { get; set; }
        static int DiagonaleMovesInFifthWay { get; set; }
        static int SixthFuel { get; set; }
        static int DiagonaleMovesInSixthWay { get; set; }
        /// <summary>
        /// 
        /// </summary>
        static int SeventhFuel { get; set; }
        static int DiagonaleMovesInSeventhWay { get; set; }
        /// <summary>
        /// The minimum amount of fuel out of 4 options
        /// </summary>
        public static int MinimumFuel { get; set; }
        /// <summary>
        /// Amount of steps of the best way
        /// </summary>
        public static int Steps { get; set; }
        /// <summary>
        /// Calculating the best way
        /// </summary>
        /// <param name="map">Matrix map</param>
        public static void CalculateRoverPath (string[,] map)
        {
            if (map is null)
            {
                WriteExceptionToFile (0);
                throw new System.ArgumentException ("CannotStartMovement");
            }

            if (map.Length == 0)
            {
                WriteExceptionToFile (0);
                throw new System.ArgumentNullException ("CannotStartMovement");
            }

            if (map[0, 0] == "X")
            {
                WriteExceptionToFile (0);
                throw new System.ArgumentException ("CannotStartMovement");
            }

            if (map[map.GetLength (0) - 1, map.GetLength (1) - 1] == "X")
            {
                WriteExceptionToFile (-2);
                throw new System.ArgumentException ("CannotStartMovement");
            }

            for (int i = 0; i < map.GetLength (0); i++)
            {
                for (int j = 0; j < map.GetLength (1); j++)
                {
                    if (map[i, j] != "X")
                    {
                        if (map[i, j][0] == '-' && map[i, j].Length == 2)
                        {
                            if (map[i, j][1] < 48 || map[i, j][1] > 57)
                                throw new System.ArgumentException (nameof (map));
                        }
                        else
                        {
                            if (map[i, j][0] < 48 || map[i, j][0] > 57)
                                throw new System.ArgumentException (nameof (map));
                        }
                    }
                }
            }

            int[,] numbersMap = new int[map.GetLength (0), map.GetLength (1)];
            numbersMap = CreateNumbersMap (map);
            string firstWay = BuildWay (numbersMap, 1);
            FirstFuel = CalculateFuel (numbersMap, firstWay, DiagonaleMovesInFirstWay);

            string secondWay = BuildWay (numbersMap, 2);
            SecondFuel = CalculateFuel (numbersMap, secondWay, DiagonaleMovesInSecondWay);

            string thirdWay = BuildWay (numbersMap, 3);
            ThirdFuel = CalculateFuel (numbersMap, thirdWay, DiagonaleMovesInThirdWay);

            string fourthWay = BuildWay (numbersMap, 4);
            FourthFuel = CalculateFuel (numbersMap, fourthWay, DiagonaleMovesInFourthWay);

            string fifthWay = BuildWay (numbersMap, 5);
            FifthFuel = CalculateFuel (numbersMap, fifthWay, DiagonaleMovesInFifthWay);

            string sixthWay = BuildWay (numbersMap, 6);
            SixthFuel = CalculateFuel (numbersMap, sixthWay, DiagonaleMovesInSixthWay);

            string seventhWay = BuildWay (numbersMap, 7);
            SeventhFuel = CalculateFuel (numbersMap, seventhWay, DiagonaleMovesInSeventhWay);

            int[] fuels = { FirstFuel, SecondFuel, ThirdFuel, FourthFuel, FifthFuel, SixthFuel, SeventhFuel };
            string[] ways = { firstWay[..^2], secondWay[..^2], thirdWay[..^2], fourthWay[..^2], fifthWay[..^2], sixthWay[..^2], seventhWay[..^2] };
            int[] diagonaleMoves = { DiagonaleMovesInFirstWay, DiagonaleMovesInSecondWay,DiagonaleMovesInThirdWay,
                DiagonaleMovesInFourthWay, DiagonaleMovesInFifthWay, DiagonaleMovesInSixthWay, DiagonaleMovesInSeventhWay };

            int indexMinimumFuel;
            FindTheBestWay (fuels, out indexMinimumFuel);
            Steps = numbersMap.GetLength (0) + numbersMap.GetLength (1) - 2 - diagonaleMoves[indexMinimumFuel];
            WriteToFile (ways, indexMinimumFuel);
        }

        private static int[,] CreateNumbersMap (string[,] map)
        {
            int[,] result = new int[map.GetLength (0), map.GetLength (1)];
            for (int i = 0; i < map.GetLength (0); i++)
            {
                for (int j = 0; j < map.GetLength (1); j++)
                {
                    result[i, j] = ConvertToInt (map[i, j]);
                }
            }

            return result;
        }

        private static int ConvertToInt (string number)
        {
            if (number[0] == '-')
            {
                return number[1] - 48 * -1;
            }
            return number[0] - 48;
        }

        /// <summary>
        /// Build way
        /// </summary>
        /// <param name="map">Matrix map</param>
        /// <param name="numberWay">number way</param>
        /// <returns></returns>
        private static string BuildWay (int[,] map, int numberWay)
        {
            string way = string.Empty;
            switch (numberWay)
            {
                case 1:
                    way = $"[0,0]->";
                    for (int r = 0, c = 0; r < map.GetLength (0) && c < map.GetLength (1);)
                    {
                        if (c == map.GetLength (1) - 1 && r == map.GetLength (0) - 1)
                        {
                            break;
                        }

                        if (r == map.GetLength (0) - 1)
                        {
                            way += $"[{r},{++c}]->";
                        }
                        else
                        {
                            if (c == map.GetLength (1) - 1)
                            {
                                way += $"[{++r},{c}]->";
                            }
                            else
                            {
                                if (map[r + 1, c] == 40 && map[r, c + 1] == 40 && map[r + 1, c + 1] == 40)
                                {
                                    WriteExceptionToFile (-1);
                                    throw new System.ArgumentException ("CannotStartMovement");
                                }

                                if ((Abs (map[r, c + 1] - map[r, c]) >= Abs (map[r + 1, c] - map[r, c])) && (Abs (map[r + 1, c + 1] - map[r, c]) >= Abs (map[r + 1, c] - map[r, c])))
                                    way += $"[{++r},{c}]->";
                                else
                                {
                                    if ((Abs (map[r, c + 1] - map[r, c]) < Abs (map[r + 1, c] - map[r, c])) && (Abs (map[r, c + 1] - map[r, c]) < Abs (map[r + 1, c + 1] - map[r, c])))
                                        way += $"[{r},{++c}]->";
                                    else
                                    {
                                        way += $"[{++r},{++c}]->";
                                        DiagonaleMovesInFirstWay++;
                                    }

                                }
                            }
                        }
                    }
                    break;
                case 2:
                    way = $"[0,0]->";
                    for (int r = 0, c = 0; r < map.GetLength (0) && c < map.GetLength (1);)
                    {
                        if (c == map.GetLength (1) - 1 && r == map.GetLength (0) - 1)
                        {
                            break;
                        }

                        if (r == map.GetLength (0) - 1)
                        {
                            way += $"[{r},{++c}]->";
                        }
                        else
                        {
                            if (c == map.GetLength (1) - 1)
                            {
                                way += $"[{++r},{c}]->";
                            }
                            else
                            {
                                if (map[r + 1, c] == 40 && map[r, c + 1] == 40 && map[r + 1, c + 1] == 40)
                                {
                                    WriteExceptionToFile (-1);
                                    throw new System.ArgumentException ("CannotStartMovement");
                                }

                                if ((Abs (map[r, c + 1] - map[r, c]) <= Abs (map[r + 1, c] - map[r, c])) && (Abs (map[r, c + 1] - map[r, c]) <= Abs (map[r + 1, c + 1] - map[r, c])))
                                    way += $"[{r},{++c}]->";
                                else
                                {
                                    if ((Abs (map[r, c + 1] - map[r, c]) > Abs (map[r + 1, c] - map[r, c])) && (Abs (map[r + 1, c + 1] - map[r, c]) > Abs (map[r + 1, c] - map[r, c])))
                                        way += $"[{++r},{c}]->";
                                    else
                                    {
                                        way += $"[{++r},{++c}]->";
                                        DiagonaleMovesInSecondWay++;
                                    }

                                }
                            }
                        }
                    }
                    break;
                case 3:
                    way = $"[0,0]->";
                    for (int r = 0, c = 0; r < map.GetLength (0) && c < map.GetLength (1);)
                    {
                        if (c == map.GetLength (1) - 1 && r == map.GetLength (0) - 1)
                        {
                            break;
                        }

                        if (r == map.GetLength (0) - 1)
                        {
                            way += $"[{r},{++c}]->";
                        }
                        else
                        {
                            if (c == map.GetLength (1) - 1)
                            {
                                way += $"[{++r},{c}]->";
                            }
                            else
                            {
                                if (map[r + 1, c] == 40 && map[r, c + 1] == 40 && map[r + 1, c + 1] == 40)
                                {
                                    WriteExceptionToFile (-1);
                                    throw new System.ArgumentException ("CannotStartMovement");
                                }

                                if ((Abs (map[r, c + 1] - map[r, c]) >= Abs (map[r + 1, c + 1] - map[r, c])) && (Abs (map[r + 1, c] - map[r, c]) >= Abs (map[r + 1, c + 1] - map[r, c])))
                                {
                                    way += $"[{++r},{++c}]->";
                                    DiagonaleMovesInThirdWay++;
                                }
                                else
                                {
                                    if ((Abs (map[r, c + 1] - map[r, c]) < Abs (map[r + 1, c + 1] - map[r, c])) && (Abs (map[r, c + 1] - map[r, c]) < Abs (map[r + 1, c] - map[r, c])))
                                        way += $"[{r},{++c}]->";
                                    else
                                        way += $"[{++r},{c}]->";

                                }
                            }
                        }
                    }
                    break;
                case 4:
                    way = $"[{map.GetLength (0) - 1},{map.GetLength (1) - 1}]->";
                    for (int r = map.GetLength (0) - 1, c = map.GetLength (1) - 1; r >= 0 && c >= 0;)
                    {
                        if (c == 0 && r == 0)
                        {
                            break;
                        }

                        if (r == 0)
                        {
                            way += $"[{r},{--c}]->";
                        }
                        else
                        {
                            if (c == 0)
                            {
                                way += $"[{--r},{c}]->";
                            }
                            else
                            {
                                if (map[r - 1, c] == 40 && map[r, c - 1] == 40 && map[r - 1, c - 1] == 40)
                                {
                                    WriteExceptionToFile (-1);
                                    throw new System.ArgumentException ("CannotStartMovement");
                                }

                                if ((Abs (map[r, c - 1] - map[r, c]) >= Abs (map[r - 1, c] - map[r, c])) && (Abs (map[r - 1, c - 1] - map[r, c]) >= Abs (map[r - 1, c] - map[r, c])))
                                    way += $"[{--r},{c}]->";
                                else
                                {
                                    if ((Abs (map[r, c - 1] - map[r, c]) < Abs (map[r - 1, c] - map[r, c])) && (Abs (map[r, c - 1] - map[r, c]) < Abs (map[r - 1, c - 1] - map[r, c])))
                                        way += $"[{r},{--c}]->";
                                    else
                                    {
                                        way += $"[{--r},{--c}]->";
                                        DiagonaleMovesInFourthWay++;
                                    }

                                }
                            }
                        }
                    }
                    break;
                case 5:
                    way = $"[{map.GetLength (0) - 1},{map.GetLength (1) - 1}]->";
                    for (int r = map.GetLength (0) - 1, c = map.GetLength (1) - 1; r >= 0 && c >= 0;)
                    {
                        if (c == 0 && r == 0)
                        {
                            break;
                        }

                        if (r == 0)
                        {
                            way += $"[{r},{--c}]->";
                        }
                        else
                        {
                            if (c == 0)
                            {
                                way += $"[{--r},{c}]->";
                            }
                            else
                            {
                                if (map[r - 1, c] == 40 && map[r, c - 1] == 40 && map[r - 1, c - 1] == 40)
                                {
                                    WriteExceptionToFile (-1);
                                    throw new System.ArgumentException ("CannotStartMovement");
                                }

                                if ((Abs (map[r, c - 1] - map[r, c]) <= Abs (map[r - 1, c] - map[r, c])) && (Abs (map[r, c - 1] - map[r, c]) <= Abs (map[r - 1, c - 1] - map[r, c])))
                                    way += $"[{r},{--c}]->";
                                else
                                {
                                    if ((Abs (map[r, c - 1] - map[r, c]) > Abs (map[r - 1, c] - map[r, c])) && (Abs (map[r - 1, c - 1] - map[r, c]) > Abs (map[r - 1, c] - map[r, c])))
                                        way += $"[{--r},{c}]->";
                                    else
                                    {
                                        way += $"[{--r},{--c}]->";
                                        DiagonaleMovesInFifthWay++;
                                    }

                                }
                            }
                        }
                    }
                    break;
                case 6:
                    way = $"[{map.GetLength (0) - 1},{map.GetLength (1) - 1}]->";
                    for (int r = map.GetLength (0) - 1, c = map.GetLength (1) - 1; r >= 0 && c >= 0;)
                    {
                        if (c == 0 && r == 0)
                        {
                            break;
                        }

                        if (r == 0)
                        {
                            way += $"[{r},{--c}]->";
                        }
                        else
                        {
                            if (c == 0)
                            {
                                way += $"[{--r},{c}]->";
                            }
                            else
                            {
                                if (map[r - 1, c] == 40 && map[r, c - 1] == 40 && map[r - 1, c - 1] == 40)
                                {
                                    WriteExceptionToFile (-1);
                                    throw new System.ArgumentException ("CannotStartMovement");
                                }

                                if ((Abs (map[r, c - 1] - map[r, c]) >= Abs (map[r - 1, c - 1] - map[r, c])) && (Abs (map[r - 1, c] - map[r, c]) >= Abs (map[r - 1, c - 1] - map[r, c])))
                                {
                                    way += $"[{--r},{--c}]->";
                                    DiagonaleMovesInSixthWay++;
                                }
                                else
                                {
                                    if ((Abs (map[r, c - 1] - map[r, c]) < Abs (map[r - 1, c - 1] - map[r, c])) && (Abs (map[r, c - 1] - map[r, c]) < Abs (map[r - 1, c] - map[r, c])))
                                        way += $"[{r},{--c}]->";
                                    else
                                        way += $"[{--r},{c}]->";
                                }
                            }
                        }
                    }
                    break;
                case 7:
                    way = $"[{map.GetLength (0) - 1},{map.GetLength (1) - 1}]->";
                    for (int r = map.GetLength (0) - 1, c = map.GetLength (1) - 1; r >= 0 && c >= 0;)
                    {
                        if (c == 0 && r == 0)
                        {
                            break;
                        }

                        if (map[r - 1, c - 1] == 40)
                        {
                            way = "000";
                            break;
                        }

                        way += $"[{--r},{--c}]->";
                        DiagonaleMovesInSeventhWay++;
                    }
                    break;
            }

            return way;
        }

        /// <summary>
        /// taking a number modulo
        /// </summary>
        /// <param name="number">number</param>
        /// <returns></returns>
        private static int Abs (int number)
        {
            if (number < 0)
            {
                return number * -1;
            }

            return number;
        }

        /// <summary>
        /// Fuel calculate for each way
        /// </summary>
        /// <param name="map">matrix map</param>
        /// <param name="way">way</param>
        /// <returns></returns>
        private static int CalculateFuel (int[,] map, string way, int diagonaleMoves)
        {
            int fuel = 0;
            if (way == "000")
            {
                return -1;
            }

            for (int i = 1; i < way.Length - 7; i += 7)
            {
                fuel += Abs (map[way[i] - 48, way[i + 2] - 48] - map[way[i + 7] - 48, way[i + 9] - 48]) + 1;
            }

            fuel += diagonaleMoves / 2;

            return fuel;
        }

        /// <summary>
        /// Find the best way out of the proposed
        /// </summary>
        /// <param name="fuels">array of proposed fuel options</param>
        /// <param name="indexMinimumFuel">the best way index</param>
        private static void FindTheBestWay (int[] fuels, out int indexMinimumFuel)
        {
            MinimumFuel = FirstFuel;
            indexMinimumFuel = 0;

            for (int i = 1; i < 7; i++)
            {
                if (fuels[i] == -1)
                    continue;

                if (MinimumFuel > fuels[i])
                {
                    MinimumFuel = fuels[i];
                    indexMinimumFuel = i;
                }
            }
        }

        /// <summary>
        /// Write data to file at current directory
        /// </summary>
        /// <param name="ways"></param>
        /// <param name="indexMinimumFuel"></param>
        static void WriteToFile (string[] ways, int indexMinimumFuel)
        {
            try
            {
                using (StreamWriter file = new StreamWriter (path, false, System.Text.Encoding.Default))
                {
                    file.WriteLine (ways[indexMinimumFuel]);
                    file.WriteLine ($"steps:{Steps}");
                    file.Write ($"fuel:{MinimumFuel}");
                }
            }
            catch
            {
                throw new System.Exception (nameof (CalculateRoverPath));
            }
        }

        static void WriteExceptionToFile (int exception)
        {
            try
            {
                using (StreamWriter file = new StreamWriter (path, false, System.Text.Encoding.Default))
                {
                    if (exception == -1)
                        file.WriteLine ("Cannot continue move because the way is blo�ked.");
                    else
                    {
                        if (exception == -2)
                            file.WriteLine ("The final point is impossible to reach.");
                        else
                            file.WriteLine ("Cannot start a movement because cannot reach the starting point");
                    }
                }
            }
            catch
            {
                throw new System.Exception (nameof (CalculateRoverPath));
            }
        }
    }
}
